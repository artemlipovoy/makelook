import React from "react";

import {Configurator} from "./pages/Configurator/Configurator";
import {Header} from "./components/Header/Header";
import {Container} from "./components/ui/Container";


function App() {
  return <Container>
    <Header />
    <Configurator />
  </Container>
}

export default App;

