import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getRandomLook, loadCollections} from "../../store/actions";
import {Viewer} from "../../components/clothes/Viewer/Viewer";
import {Button, Col, Row} from "react-materialize";
import {ConfiguratorWrapper} from "./ConfiguratorWrapper";
import {Card} from "../../components/clothes/Card/Card";
import {Loader} from "../../components/ui/Loader";
import randomIcon from "../../assets/img/randomIcon.svg";
import {ProgressBar} from "../../components/ui/ProgressBar";


export const Configurator = () => {
    const dispatch = useDispatch()
    const lookIds = useSelector(state=>state.clothes.lookIds)

    const loadedLook = useSelector(state=>state.clothes.loadedLook)
    const loading = useSelector(state=>state.app.loading)
    const currentModel = useSelector(state=>state.clothes.loadedModel)

    useEffect(()=>{
        dispatch(getRandomLook())
        dispatch(loadCollections())
    },[dispatch])

    if(!loadedLook){
        return <Loader />
    }
    return <>
        <ConfiguratorWrapper>
            <Col
                className="configuratorViewer"
                s={12}
                l={6}>
                <Viewer ids={lookIds} />
            </Col>
            <Col
                s={12}
                l={6}>
                <div className="configuratorHeader">
                    <h6><b>Детали лука</b></h6>
                    <Button
                        className="waves-effect"
                        flat
                        onClick={()=>dispatch(getRandomLook(currentModel))}>
                        <img src={randomIcon} alt="Random look" />
                        Собрать случайный
                    </Button>
                </div>
                <Row className="configuratorClothesList">
                    {loadedLook.length&&loading?<ProgressBar />:null}
                    {
                        !loadedLook.length?
                            <Loader />:
                            loadedLook.map(v=><Card
                                key={`card_${v.ID}`}
                                clothes={v.clothes[0]}
                                availableSizes={v.available_sizes}
                                layer = {v.layer}
                                size={v.size[0]}/>)
                    }
                </Row>
            </Col>
        </ConfiguratorWrapper>
    </>
}