import {Row} from "react-materialize";
import styled from "styled-components";

export const ConfiguratorWrapper = styled(Row)`
  display: flex;
  flex-wrap: wrap;

  .col, .row, h6{
    margin: 0;
    padding: 0;
  }
  
  .material-icons{
    margin: 0;
  }
  
  .configuratorViewer{
    border: 1px solid black;
    border-right: none;
  }
  
  .configuratorHeader{
      display: flex;
      align-items: center;
      justify-content: space-between;
      padding: 14px;
      h6{
        font-size: 20px;
      }
      button{
        display: flex;
        align-items: center;
        font-weight: 300;
        text-transform: none;
        font-size: 14px;
        padding: 0;
      }
      img{
        margin-right: 8px;
      }
      border: 1px solid black;
  }
  
  .configuratorClothesList{
    min-height: 522px;
    border-right: 1px solid black;
    border-left: 1px solid black;
  }
  
  @media screen and (max-width: 992px){
    .configuratorViewer{
      border-right: 1px solid black;
      height: 532px!important;
      .modelActions{
        border-bottom: 1px solid black;
      }
  }
    }
    
    .configuratorHeader{
      border-right: 1px solid black;
      border-top: 1px solid black;
    }
      
  }
  
  @media screen and (max-width: 576px) {
      .configuratorViewer{
          border-left: none;
          border-right: none;
      }
      .configuratorHeader{
          border-right: none;
          border-left: none;
      }
      .configuratorClothesList{
          border-right: none;
          border-left: none;
      }
  }
  
`