import {ADD_TO_CART, CHANGE_QUANTITY} from "../types";

const initialState = {
    totalSum: 0,
    items: {}
}

export const cartReducer = (state = initialState, action) => {
    let newState, totalSum
    switch (action.type) {
        case ADD_TO_CART:
            newState = {
                ...state,
                items: {
                    ...state.items,
                    ...action.payload
                }
            }
            totalSum = Object.values(newState.items).reduce((value, element)=>value+element.product.price*element.quantity,0)
            newState.totalSum = totalSum
            return newState
        case CHANGE_QUANTITY:
            newState = {
                ...state,
                items: {
                    ...state.items,
                    [action.payload.id]: {
                        ...state.items[action.payload.id],
                        quantity: action.payload.quantity
                    }
                }
            }
            totalSum = Object.values(newState.items).reduce((value, element)=>value+element.product.price*element.quantity,0)
            newState.totalSum = totalSum
            return newState
        default: return state
    }
}
