import {LOAD_COLLECTIONS} from "../types";

const initialState = {
    sizes: {
        shoes: [],
        clothes: [],
    },
    models: [],
    compositions: [],
    colors: []
}

export const collectionsReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_COLLECTIONS:
            return {
                ...state,
                ...action.payload
            }
        default: return state
    }
}
