import {CHANGE_MODEL, LOAD_LOOK, LOAD_RANDOM_LOOK, ROTATE_LOOK, SET_LOOK_IDS} from "../types";

const initialState = {
    lookIds: [],
    loadedLook: [],
    lookAngle: 0,
    loadedModel: null
}

export const clothesReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_LOOK_IDS:
            return {
                ...state,
                lookIds: action.payload
            }
        case LOAD_LOOK:
            let loadedLook = action.payload.sort((a,b)=>a.layer<b.layer?1:-1)
            loadedLook[loadedLook.length-1] = loadedLook.splice(loadedLook.length-2, 1, loadedLook[loadedLook.length-1])[0];
            return {
                ...state,
                loadedModel: !state.loadedModel?action.payload[0].model[0].ID:state.loadedModel,
                loadedLook
            }
        case ROTATE_LOOK:
            return {
                ...state,
                lookAngle: action.payload
            }
        case LOAD_RANDOM_LOOK:
            return {
                ...state,
                lookIds: action.payload
            }
        case CHANGE_MODEL:
            return {
                ...state,
                loadedModel: action.payload
            }
        default: return state
    }
}
