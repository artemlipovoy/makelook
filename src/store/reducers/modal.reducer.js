import {HIDE_MODAL, SHOW_MODAL} from "../types";

const initialState = {}

export const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_MODAL:
            return {
                ...state,
                [action.payload]: true
            }
        case HIDE_MODAL:
            return {
                ...state,
                [action.payload]: false
            }
        default: return state
    }
}