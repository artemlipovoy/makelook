import axios from 'axios'
import qs from 'qs'
import {
    ADD_TO_CART,
    CHANGE_MODEL, CHANGE_QUANTITY,
    HIDE_LOADER, HIDE_MODAL,
    LOAD_COLLECTIONS,
    LOAD_LOOK,
    LOAD_RANDOM_LOOK,
    ROTATE_LOOK, SET_LOOK_IDS,
    SHOW_LOADER, SHOW_MODAL
} from "./types";
import {useNotify} from "../components/ui/useNotify";


export function showLoader() {
    return {
        type: SHOW_LOADER
    }
}
export function hideLoader() {
    return {
        type: HIDE_LOADER
    }
}

export function showModal(id){
    return {
        type: SHOW_MODAL,
        payload: id
    }
}

export function hideModal(id){
    return {
        type: HIDE_MODAL,
        payload: id
    }
}

export function rotateLook(angle) {
    return {
        type: ROTATE_LOOK,
        payload: angle
    }

}

export function changeModel(id) {
    return {
        type: CHANGE_MODEL,
        payload: id
    }
}

export function setLookIds(ids) {
    return {
        type: SET_LOOK_IDS,
        payload: ids
    }
}

export function getRandomLook(model=null) {
    return async dispatch => {
        try{
            dispatch(showLoader())
            const response = await axios.get(`/rest/clothes.getrandomlook`, {
                params: {model},
            })
            dispatch({
                type: LOAD_RANDOM_LOOK,
                payload: response.data.result
            })
            dispatch(hideLoader())
        }catch(e){
            dispatch(hideLoader())
            console.log(e)
        }
    }
}

export function loadLookByIds(ids) {
    return async dispatch => {
        try{
            dispatch(showLoader())
            const response = await axios.get(`/rest/clothes.photos.list`, {
                params: {
                    filter: {
                        'ID': ids
                    }
                },
                paramsSerializer: params => {
                    return qs.stringify(params)
                }
            })
            dispatch({
                type: LOAD_LOOK,
                payload: response.data.result
            })
            dispatch(hideLoader())
        }catch(e){
            dispatch(hideLoader())
            console.log(e)
        }
    }
}

export function loadCollections() {
    return async dispatch => {
        try{
            dispatch(showLoader())
            const response = await axios.get(`/rest/collections.getClothesCollections`)
            dispatch({
                type: LOAD_COLLECTIONS,
                payload: response.data.result
            })
            dispatch(hideLoader())
        }catch(e){
            dispatch(hideLoader())
            console.log(e)
        }
    }
}

export function loadRandomClothes(layer, loadedLook, model){
    return async dispatch => {
        try{
            dispatch(showLoader())
            const response = await axios.get(`/rest/clothes.getrandomclothes`, {
                params: {
                    layer,
                    model
                },
            })

            let replacedKey = null;
            const newItem = response.data.result
            loadedLook.forEach((v,k)=>v.layer===layer?replacedKey=k:null)
            loadedLook[replacedKey] = newItem
            dispatch({
                type: LOAD_LOOK,
                payload: loadedLook
            })
            dispatch(hideLoader())
        }catch(e){
            dispatch(hideLoader())
            console.log(e)
        }
    }
}

export function loadAnotherSize(clothes, size, loadedLook, model){
    return async dispatch => {
        try{
            dispatch(showLoader())
            const response = await axios.get(`/rest/clothes.photos.list`, {
                params: {
                    filter: {
                        clothes,
                        size,
                        model
                    }
                },
                paramsSerializer: params => {
                    return qs.stringify(params)
                }
            })
            let replacedKey = null;

            const newItem = response.data.result[0]
            loadedLook.forEach((v,k)=>v.clothes[0].ID===newItem.clothes[0].ID?replacedKey=k:null)
            loadedLook[replacedKey] = newItem
            dispatch({
                type: LOAD_LOOK,
                payload: loadedLook
            })
            dispatch(hideLoader())
        }catch(e){
            dispatch(hideLoader())
            console.log(e)
        }
    }
}

export function addToCart(product, size) {
    return (dispatch, getState)=>{
        const {notify, DANGER_NOTIFY, SUCCESS_NOTIFY, INFO_NOTIFY} = useNotify()
        if(!size){
            notify('Сначала выбери размер', DANGER_NOTIFY)
        }else if(Object.keys(getState().cart.items).includes(product.ID)){
            notify('Эта вещь уже в корзине', INFO_NOTIFY)
        } else {
            dispatch({
                type: ADD_TO_CART,
                payload: {
                    [product.ID]: {
                        product,
                        size,
                        quantity: 1
                    }
                }
            })
            notify('Вещь добавлена в корзину', SUCCESS_NOTIFY)
        }
    }
}
export function changeQuantity(id, quantity) {
    return {
        type: CHANGE_QUANTITY,
        payload: {
            id,
            quantity
        }
    }
}