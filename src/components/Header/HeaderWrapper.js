import styled from "styled-components";

export const HeaderWrapper = styled.div`
    .row{
      margin: 13px 0;
    }
    .headerDeliveryInfo{
      display: inline-block;
      font-size: 12px;
      line-height: 14px;
    }
`