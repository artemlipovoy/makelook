import {Col, Row} from "react-materialize";
import logo from "../../assets/img/logo.svg"
import React from "react";
import {PlainText} from "../ui/PlainText";
import {useDispatch, useSelector} from "react-redux";
import {Cart} from "../Cart/Cart";
import {showModal} from "../../store/actions";
import {HeaderWrapper} from "./HeaderWrapper";

export const Header = () => {
    const dispatch = useDispatch()
    const cartData = useSelector(state=>state.cart)
    const count = Object.keys(cartData.items).length
    let itemsCase = ''
    switch(count){
        case 1: itemsCase = 'вещь'; break
        case 2:
        case 3:
        case 4: itemsCase=  'вещи'; break
        default: itemsCase = 'вещей'; break
    }
    return <HeaderWrapper>
        <Cart />
        <Row className="valign-wrapper">
            <Col>
                <img src={logo} alt={"MakeLook logo"} />
            </Col>
            <Col className="hide-on-med-and-down">
                <span className="headerDeliveryInfo">
                    Доставка одежды из магазинов города<br/>
                    за 4 часа при заказе с 10 до 18
                </span>
            </Col>
            <Col
                s={6}
                className="right-align">
                {count>0?
                    <PlainText
                        underline
                        pointer
                        onClick={()=>dispatch(showModal('cart'))}>
                        В корзине {count} {itemsCase} на {cartData.totalSum.toLocaleString()} рублей
                    </PlainText>
                    :
                    <PlainText underline>
                        Корзина пуста
                    </PlainText>}
            </Col>
        </Row>
    </HeaderWrapper>
}