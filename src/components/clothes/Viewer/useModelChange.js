import {useState, useCallback} from "react";
import axios from 'axios'
import {useDispatch} from "react-redux";
import {changeModel, setLookIds, showModal} from "../../../store/actions";

export const useModelChange = () => {

    const dispatch = useDispatch()
    const [onApproveChanging, setOnApproveChanging] = useState(()=>()=>{})


    const tryChangeModel = useCallback(async (elements,modelId) => {
        try {
            const response = await axios.get(`/rest/clothes.loadForAnotherModel`, {
                params: {
                    elements,
                    modelId
                }
            })
            const {withNew, newIds} = response.data.result;

            const updateClothes = () => {
                dispatch(changeModel(modelId))
                dispatch(setLookIds(newIds))
            }

            if(withNew){
                dispatch(showModal('showModelChangeConfirmModal'))
                setOnApproveChanging(()=>()=>updateClothes())
            }else{
                updateClothes()
            }
        }catch (e) {
            console.log(e)
        }
    },[dispatch])

    return {tryChangeModel, onApproveChanging}
}