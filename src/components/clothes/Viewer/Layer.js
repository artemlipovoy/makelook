import styled from "styled-components";

export const Layer = styled.div`
  width: 200px;
  height: 520px;
  z-index: ${props=>props.layer};
  position: absolute;
  background-image: url(${props=>props.sprites});
  background-repeat: no-repeat;
  background-position: ${props=>props.position}px 0;
`
