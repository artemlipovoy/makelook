import styled from "styled-components";
import {Button as MButton} from "react-materialize";

export const Button = styled(MButton)`
  width: 100%;
  height: 100%;
  box-shadow: none;
  
  line-height: unset;
  padding: 9px;
  font-size: 12px; 
  font-weight: 300;
  text-transform: none;
  border-top: 1px solid black;
  border-radius: unset;
  ${props=>props.type==='l'&&'border-right: 1px solid black;'}
  
  :hover{
    box-shadow: none;
  }
`