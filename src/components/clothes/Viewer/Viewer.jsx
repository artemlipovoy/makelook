import React, {useEffect} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {loadLookByIds, rotateLook} from "../../../store/actions";
import {Layer} from "./Layer";
import {ViewerWrapper} from "./ViewerWrapper";
import {Row, Col} from "react-materialize";
import {Button} from "./Button";
import {ModelDetails} from "./ModelDetails";
import {Loader} from "../../ui/Loader";
import {ModalDialog} from "../../ui/ModalDialog/ModalDialog";
import {useModelChange} from "./useModelChange";


export const Viewer = props => {
    //Configuration
    const framesCount = 8
    const imageWidth = 1600

    const dispatch = useDispatch()
    const loadedLook = useSelector(state=>state.clothes.loadedLook)
    const loading = useSelector(state=>state.app.loading)
    const models = useSelector(state=>state.collections.models||[])
    const currentModel = useSelector(state=>state.clothes.loadedModel)
    const curFrame = useSelector(state=>state.clothes.lookAngle)
    const {tryChangeModel, onApproveChanging} = useModelChange()

    const position = -imageWidth/framesCount*curFrame
    const model = models.find(v=>v.ID===currentModel)||{}

    useEffect(()=>{
        if(props.ids.length) {
            dispatch(loadLookByIds(props.ids))
        }
    }, [dispatch, props.ids])

    const calculateNextFrame = (curFrame, dir) => curFrame + dir > framesCount - 1 ? 0 : curFrame + dir < 0 ? framesCount - 1 : curFrame + dir;

    if(loading || !loadedLook.length){
        return <Loader />
    }

    return <>
        <ModalDialog
            id="showModelChangeConfirmModal"
            header="Продолжить?"
            onApprove={onApproveChanging}
        >
            У выбранной модели отсутствуют фотографии некоторых из выбранных вещей.
            При продолжении отсутствующие вещи будут заменены на случайные.
        </ModalDialog>
        <ViewerWrapper>
            <Row className="modelViewContainer">
                <div>
                    <div className="modelView valign-wrapper">
                        {loadedLook.map(v=><Layer key={v.ID} position={position} layer={v.layer} sprites={v.sprites} />)}
                    </div>
                </div>
            </Row>
            <ModelDetails>
                <div>
                    <h6>Параметры модели</h6>
                    <Row>
                        {models.map(v=><Col key={`model_${v.ID}`}>
                            <input
                                onChange={e=>tryChangeModel(props.ids, e.target.value)}
                                checked={v.ID===model.ID}
                                type="radio"
                                value={v.ID}
                                id={`model_${v.ID}`}
                                name='model_selector'/>
                            <label htmlFor={`model_${v.ID}`}>
                                <div>{v.height}<br/>СМ</div>
                            </label>
                        </Col>)}
                    </Row>
                    <span className="param">Охват груди <span className="paramValue">{model.chest}</span></span>
                    <span className="param">Талия <span className="paramValue">{model.waist}</span></span>
                    <span className="param">Охват бедер <span className="paramValue">{model.hips}</span></span>
                    <span className="param">Размер ноги <span className="paramValue">{model.foot_size}</span></span>
                </div>
            </ModelDetails>
            <Row className="modelActions">
                <Col s={6} className="right-align">
                    <Button
                        className="white black-text waves-effect"
                        type={'l'}
                        onClick={()=>dispatch(rotateLook(calculateNextFrame(curFrame,1)))}
                    >Повернуть влево</Button>
                </Col>
                <Col s={6} className="left-align">
                    <Button
                        className="white black-text waves-effect"
                        onClick={()=>dispatch(rotateLook(calculateNextFrame(curFrame,-1)))}
                    >Повернуть вправо</Button>
                </Col>
            </Row>
        </ViewerWrapper>
    </>

}