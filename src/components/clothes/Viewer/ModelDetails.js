import styled from "styled-components";

export const ModelDetails = styled.div`
  position: relative;
  
  > div{
    position: absolute;
    bottom: 6px;
    margin: 0 0 16px 16px;
  }
 
  margin-left: 6px;
  
  h6{
    margin-bottom: 6px!important;
    color: black;
    opacity: 0.64;
    font-size: 10px;
    text-transform: uppercase;
  }
  
  .col label > div {
    display: inline-block;
    margin: 4px 4px 4px 0;
    padding: 1px 3px;
    border: 1px solid #c7c7c7;
    color: black;
    text-align: center;
  }
  
  .col input:checked + label > div{
    border: 1px solid black;
  }
  
  .param{
    display: block;
    font-size: 10px;
  }
  
  .paramValue{
    font-size: 12px;
  }
`
