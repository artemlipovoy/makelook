import styled from "styled-components";


export const ViewerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  
  .modelViewContainer{
    margin: auto !important;
    width: 100%;
  }
  
  .modelView{
   position: relative;
   width: 200px;
   height: 500px;
   margin: auto;
  }
`