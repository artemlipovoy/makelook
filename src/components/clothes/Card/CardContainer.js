import styled from "styled-components";
import {Row} from "react-materialize";

export const CardContainer = styled(Row)`
  
  border-bottom: 1px solid black;
  
  display: flex;
  flex-wrap: wrap;
  
  > .col{
    padding: 16px;
  }
  
  > .col:nth-child(2){
    border-left: 1px solid black;
  }
  
  b{
    text-transform: uppercase;
  }
  
  h6{
      padding: 8px 0;
      font-style: normal;
      font-weight: 300;
      font-size: 0.7em;
      text-transform: uppercase;
  }
  
  h5{
      font-size: 18px;
      margin: 0;
  }
  
  h5::after{
    content: " ₽";
  }
  
  .showDetailCardArea{
    cursor: pointer;
    h6.type{
        padding: 0;
        font-size: 10px;
        font-weight: 700;
    }
    h6.name{
        padding: 0;
        font-size: 14px;
        font-weight: 300;
        text-transform: none;
        margin: 9px 0 12px 0;
    }
  }
  .clothesSizesContainer{
    margin-top: 12px;
  
    h6{
      font-size: 10px;
      color: #000000;
      opacity: 0.64;
      padding: 0;
      margin-bottom: 7px;
    }
  
    > .col{
        font-style: normal;
        font-weight: 300;
        margin: 0 4px 4px 0;
    }
    
    label > div {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 30px;
        height: 30px;
        border: 1px solid #c7c7c7;
        color: black;
        font-size: 12px;
    }
    
    input:checked + label > div{
      border: 1px solid black;
    }
    
    input:disabled + label > div{
        background-image: linear-gradient(to top right, transparent 48%, #c7c7c7, transparent 52%);
        background-repeat: no-repeat;
        border: 1px solid #c7c7c7 !important;
        color: #c7c7c7
    }
  }
 
  .cardAddToCartContainer,
  .cardActionsContainer{
    margin: 10px 0;
  }
  .cardActionsContainer{
    button{
        text-transform: none!important;
        border-radius: 0;
        font-style: normal;
        font-weight: 300;
        box-shadow: none;
        font-size: 12px;
        padding: 8px 12px;
        line-height: unset;
        border: 1px solid black;
    }
    button.selectAnother:hover{
        background-color: #FFFFFF!important;
        color: #000000!important;
    }
    button.btn-flat{
        border: none;
        text-decoration: underline;
    }
  }
  
  .cardAddToCartContainer {
    button{
        font-size: 12px;
        font-weight: 300;
        text-transform: none!important;
        border: 1px solid black;
        border-radius: 0;
        box-shadow: none;
        
        overflow: visible;
        white-space: nowrap;
    }
    button:hover{
        background-color: #000000!important;
        border: 1px solid black;
        color: #FFFFFF!important;
    }
  }
  @media screen and (max-width: 576px) {
      button.btn-flat{
          display: block;
          border: none;
          padding: 8px 0;
          text-decoration: underline;
      }
  }
`