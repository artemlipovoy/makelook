import {CardContainer} from "./CardContainer";
import React, {useRef} from "react";
import {Button, Col, Row} from "react-materialize";
import {useDispatch, useSelector} from "react-redux";
import {addToCart, loadAnotherSize, loadRandomClothes, showModal} from "../../../store/actions";
import {Catalog} from "../Catalog/Catalog";
import {DetailCard} from "../DetailCard/DetailCard";

export const Card = props => {
    const dispatch = useDispatch()

    const selectedSize = useRef(null)

    const loadedLook = useSelector(state=>state.clothes.loadedLook)
    const collections = useSelector(state=>state.collections)
    const currentModel = useSelector(state=>state.clothes.loadedModel)

    const availableSizes = props.clothes.size;
    const allSizes = collections.sizes[availableSizes[0].type]

    return <>
        <CardContainer>
            <Col s={6} l={8}>
                <DetailCard
                    id={`clothes_photos_${props.clothes.ID}`}
                    {...props.clothes}
                />
                <div
                    className="showDetailCardArea"
                    onClick={()=>dispatch(showModal(`clothes_photos_${props.clothes.ID}`))}>
                    <h6 className="type">
                        {props.clothes.category[0].name_single||props.clothes.category[0].name}
                    </h6>
                    <h6 className="name">
                        {props.clothes.name}
                    </h6>
                </div>
                <Row
                    className="clothesSizesContainer"
                    onChange={e=>dispatch(loadAnotherSize(props.clothes.ID, e.target.value, loadedLook, currentModel))}>
                    <h6>Размер на модели</h6>
                    {props.availableSizes.map(allValue=>
                        <Col key={allValue.ID}>
                            <input
                                defaultChecked={allValue.ID===props.size.ID}
                                type="radio"
                                value={allValue.ID}
                                id={`model_${props.clothes.ID}_${allValue.ID}`}
                                name={`model_${props.clothes.ID}`} />
                            <label htmlFor={`model_${props.clothes.ID}_${allValue.ID}`}>
                                <div>{allValue.size}</div>
                            </label>
                        </Col>
                    )}
                </Row>
                <Row className="cardActionsContainer">
                    <Col>
                        <Button
                            onClick={()=>dispatch(showModal(`catalog_layer_${props.layer}`))}
                            className="waves-effect waves-light black white-text selectAnother">
                            Выбрать другую
                        </Button>
                    </Col>
                    <Col>
                        <Button
                            flat
                            onClick={()=>dispatch(loadRandomClothes(props.layer, loadedLook, currentModel))}>
                            Случайная
                        </Button>
                    </Col>
                </Row>
            </Col>
            <Col s={6} l={4}>
                <h5>
                    {Number(props.clothes.price).toLocaleString()}
                </h5>
                <Row
                    className="clothesSizesContainer"
                    onChange={e=>selectedSize.current = e.target.value}>
                    <h6>Размеры в наличии</h6>
                    {allSizes.map(allValue=>
                        <Col key={allValue.ID}>
                            <input
                                disabled={!availableSizes.some(availableValue=>allValue.ID===availableValue.ID)}
                                type="radio"
                                value={allValue.ID}
                                id={`clothes_${props.clothes.ID}_${allValue.ID}`}
                                name={`clothes_${props.clothes.ID}`} />
                            <label htmlFor={`clothes_${props.clothes.ID}_${allValue.ID}`}>
                                <div>{allValue.size}</div>
                            </label>
                        </Col>
                    )}
                </Row>
                <Row>
                    <Col className="cardAddToCartContainer">
                        <Button
                            className="waves-effect white black-text"
                            onClick={()=>dispatch(addToCart(props.clothes, allSizes.find(v=>v.ID===selectedSize.current)))}>
                            В корзину
                        </Button>
                    </Col>
                </Row>
            </Col>
        </CardContainer>
        <Catalog header={props.clothes.category[0].type} layer={props.layer} id={`catalog_layer_${props.layer}`} />
    </>
}