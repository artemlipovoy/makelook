import React from 'react'
import {useDispatch, useSelector} from "react-redux";
import {hideModal} from "../../../store/actions";
import {CatalogWrapper} from "./CatalogWrapper";
import {Col, Row} from "react-materialize";
import {useClothesLayer} from "./useClothesLayer";
import {Card} from "./Card/Card.jsx";
import {Loader} from "../../ui/Loader";
import closeIcon from "../../../assets/img/closeIcon.svg";

export const Catalog = props => {

    const dispatch = useDispatch()
    const modalState = useSelector(state=>state.modals[props.id]||false)
    const {
        clothes,
        loadClothesByLayer,
        sizeFilter,
        selectedSizes,
        sizeType,
        colorFilter,
        selectedColors,
        loading
    } = useClothesLayer()
    const allSizes = useSelector(state=>state.collections.sizes[sizeType]||[])
    const allColors = useSelector(state=>state.collections.colors||[])

    return <CatalogWrapper
        actions={[]}
        id={props.id}
        open={modalState}
        innerheight={window.innerHeight}
        options={{
            onOpenStart: ()=>loadClothesByLayer(props.layer),
            onCloseEnd: ()=>{
                dispatch(hideModal(props.id))
                sizeFilter()
            }
        }}
    >
        {loading?<Loader fixVertical={1} />:<>
            <div className="closeIcon modal-close">
                <img src={closeIcon} alt="close" />
            </div>
            <div className="contentWrapper">
                <Row className="filter">
                    <Col s={12} className="header">
                        {props.header}
                    </Col>
                    <Col className="sizeFilter">
                        <span>Размер</span>
                        {allSizes.map(v=><Col key={`filter_size_${v.ID}`}>
                            <input
                                type="checkbox"
                                value={v.ID}
                                onChange={e=>sizeFilter(e.target.value,e.target.checked)}
                                checked={selectedSizes.includes(v.ID)}
                                id={`filter_size_${props.layer}_${v.ID}`} />
                            <label htmlFor={`filter_size_${props.layer}_${v.ID}`}>
                                <div>{v.size}</div>
                            </label>
                        </Col>)}
                    </Col>
                    <Col className="colorFilter">
                        <span>Цвет</span>
                        {allColors.map(v=><Col key={`filter_color_${v.ID}`}>
                            <input
                                type="checkbox"
                                value={v.ID}
                                onChange={e=>colorFilter(e.target.value,e.target.checked)}
                                checked={selectedColors.includes(v.ID)}
                                id={`filter_color_${props.layer}_${v.ID}`} />
                            <label htmlFor={`filter_color_${props.layer}_${v.ID}`}>
                                <div>
                                    <div style={{background: v.hex}} />
                                </div>
                            </label>
                        </Col>)}
                    </Col>
                </Row>
                <Row className="cardsContainer">
                    {clothes.length?clothes.map(v=><Card key={v.ID} layer={props.layer} {...v} />):<>
                        <h2 className="center-align">Ничего не найдено</h2>
                    </>}
                </Row>
            </div>
        </>}
    </CatalogWrapper>
}