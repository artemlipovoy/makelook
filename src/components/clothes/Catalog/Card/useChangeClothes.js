import {useCallback, useState} from "react";
import axios from "axios";
import {LOAD_LOOK} from "../../../../store/types";
import {useDispatch, useSelector} from "react-redux";
import {changeModel, hideModal, setLookIds, showModal} from "../../../../store/actions";

export const useChangeClothes = () => {
    const dispatch = useDispatch()
    const model = useSelector(state=>state.clothes.loadedModel)
    const loadedLook = useSelector(state=>state.clothes.loadedLook)
    const allModals = useSelector(state=>state.modals)

    const [onApproveChanging, setOnApproveChanging] = useState(()=>()=>{})

    const tryChangeClothes = useCallback(async clothes=>{
        try{
            const response = await axios.get(`/rest/clothes.insertInLook`, {
                params: {
                    clothes,
                    model
                }
            })
            const {newLook, photoData, photoIds, newModelId, layer} = response.data.result
            if(newLook){
                dispatch(showModal(`clothesChangeApprove_${clothes}`))
                setOnApproveChanging(()=>()=>{
                    dispatch(setLookIds(photoIds))
                    dispatch(changeModel(newModelId))
                    Object.keys(allModals).forEach(v=>dispatch(hideModal(v)))
                })
            }else{
                let replaceKey = null;
                const newLook = [...loadedLook]
                newLook.forEach((v,k)=>v.layer===layer?replaceKey=k:null)
                newLook[replaceKey] = photoData
                dispatch({
                    type: LOAD_LOOK,
                    payload: newLook
                })
                Object.keys(allModals).forEach(v=>dispatch(hideModal(v)))
            }
        }catch(e) {
            console.log(e)
        }
    },[dispatch, model, loadedLook, allModals])

    return {tryChangeClothes, onApproveChanging}
}