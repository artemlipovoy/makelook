import styled from "styled-components";
import {CardPanel} from "react-materialize";

export const Card = styled(CardPanel)`
  box-shadow: none;
  border: 1px solid black;
  padding: 0;
  width: 240px;
  height: 290px;
  
  .photoContainer{
    margin: 0;
    padding: 0;
    background-image: url("${props => props.image}");
    height: 150px;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    border-bottom: 1px solid black;
  }
  
  .contentContainer{
      padding: 12px;
      margin: 0;
      .productName{
        font-style: normal;
        font-weight: 300;
        text-decoration: underline;
        font-size: 14px;
      }
      .sizes{
        margin: 8px 0;
        padding: 0;
        .col{
          display: flex;
          align-items: center;
          justify-content: center;
          width: 30px;
          height: 30px;
          font-style: normal;
          font-weight: 300;
          margin: 4px 4px 4px 0;
          font-size: 12px;
          border: 1px solid #c7c7c7;
          color: black;
        }
        .col.disabled{
          background-image: linear-gradient(to top right, transparent 48%, #c7c7c7, transparent 52%);
          background-repeat: no-repeat;
          border: 1px solid #c7c7c7 !important;
          color: #c7c7c7
        }
      }
  }
  .modal-footer{
   
    background: inherit;
    padding: 0;
    margin: 2px 0 0;
    height: unset;
    
    button{
        margin: 0!important;
        text-transform: none!important;
        border-radius: 0;
        font-style: normal;
        font-weight: 300;
        box-shadow: none;
        font-size: 12px;
        padding: 8px 12px;
        line-height: unset;
        border: 1px solid black;
    }
    
    button:hover{
        background-color: #FFFFFF!important;
        color: #000000!important;
    }
    
    .price{
        margin: 0;
        padding: 8px 0;
        font-style: normal;
        font-weight: 300;
        font-size: 14px;
        text-transform: uppercase;
    }
    .price::after{
      content: ' ₽';
    }
  }
`