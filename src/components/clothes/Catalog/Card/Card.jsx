import {Card as SCard} from './Card'
import {Button, Col, Row} from "react-materialize";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {useChangeClothes} from "./useChangeClothes";
import {ModalDialog} from "../../../ui/ModalDialog/ModalDialog";
import {DetailCard} from "../../DetailCard/DetailCard";
import {showModal} from "../../../../store/actions";

export const Card = props => {
    const dispatch = useDispatch()
    const allSizes = useSelector(state=>state.collections.sizes[props.size[0].type])
    const {tryChangeClothes, onApproveChanging} = useChangeClothes()

    return <Col>
        <ModalDialog
            id={`clothesChangeApprove_${props.ID}`}
            header="Продолжить?"
            onApprove={onApproveChanging}
        >
            У этой одежды отсутствуют фотографии на модели.
            При продолжении будет сгенерирован случайный лук с выбранной одеждой.
        </ModalDialog>
        <DetailCard
            id={`clothes_catalog_${props.ID}`}
            {...props}
        />
        <SCard image={typeof props.photo === 'string'?props.photo:props.photo[0]}>
            <div className="photoContainer" />
            <div className="contentContainer" >
                <div className="productName"
                     onClick={()=>dispatch(showModal(`clothes_catalog_${props.ID}`))}
                >{props.name}</div>
                <Row className="sizes">
                    {allSizes.map(v=><Col
                        className={!props.size.some(availableValue=>v.ID===availableValue.ID)?'disabled':''}
                        key={v.ID}>
                        {v.size}
                    </Col>)}
                </Row>
                <div className="modal-footer">
                    <Button
                        className="left waves-effect waves-light black white-text"
                        onClick={()=>tryChangeClothes(props.ID)}>
                        Примерить
                    </Button>
                    <span className="price right">{Number(props.price).toLocaleString()}</span>
                </div>
            </div>
        </SCard>
    </Col>
}