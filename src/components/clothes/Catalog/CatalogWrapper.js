import styled from "styled-components";
import {Modal} from "react-materialize";

export const CatalogWrapper = styled(Modal)`
  border: 1px solid black;
  border-radius: 0;
  box-shadow: none;
  max-height: unset;
  width: 1100px;
  background-color: #FFFFFF;
  
  .modal-content{
    padding: 0;
  }
  
  .contentWrapper{
    padding: 24px;
  }
  
  .row{
    padding: 0;
    margin: 0;
  }
    
  .preloader-wrapper{
      margin-top: 17%;
  }
  
  > .modal-footer{
    display: none;
  }
 
  + .modal-overlay{
     background: #FFFFFF;
     opacity: 0.64!important;
  }
  
  h4:not(.header){
    all: unset;
  }
  
  .col.header{
    font-size: 20px;
    font-weight: 700;
  }
  
  .closeIcon{
        display: block;
        cursor: pointer;
        position: relative;
        img{
            position: absolute;
            right: 0;
        }
    }
  
  .clothesSizesContainer > .col {
    font-style: normal;
    font-weight: 300;
    margin: 4px 4px 4px 0;
  }
  
  .clothesSizesContainer label > div {
    display: inline-block;
    padding: 3px 7px;
    border: 1px solid #c7c7c7;
    color: black;
  }
  
  .clothesSizesContainer input:checked + label > div{
    border: 1px solid black;
  }
  
  .filter{
    .header{
      margin-bottom: 11px;
    }
    span{
      font-size: 10px;
      text-transform: uppercase;
      color: #000000;
      opacity: 0.64;
      display: block
    }
    .sizeFilter,
    .colorFilter{
      > .col {
        font-style: normal;
        font-size: 1.2rem!important;
        font-weight: 300;
        margin: 4px 4px 4px 0;
        padding: 0;
      }
      
      label > div {
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 12px;
        width: 30px;
        height: 30px;
        border: 1px solid #c7c7c7;
        color: black;
      }
      
      input:checked + label > div{
        border: 1px solid black;
      }
    }
    
    .colorFilter{
      label > div > div{
        width: 26px;
        height: 26px;
        margin: 2px auto;
        border-radius: 18px;
      }
    }
  }
  
  .cardsContainer{
    margin-top: 16px;
    h2{
      margin-top: 50px;
      color: #000000;
      opacity: 0.64;
    }
  }
  @media screen and (max-width: 1100px) {
    width: 836px;
  }
  
  @media screen and (max-width: 992px) {
    width: 576px;
  }
  
  @media screen and (max-width: 576px) {
    top: 0!important;
    width: 100vw;
    height: ${props=>props.innerheight}px;
    overflow: auto;
    .cardsContainer{
      display: flex;
      flex-direction: column;
      align-items: center;
    }
  }
`