import {useCallback, useEffect, useReducer, useRef, useState} from "react";
import axios from 'axios'
import qs from "qs";

const filterDispatcher = (state, action)=>{
    if(!action.id)
        return []
    if(action.state){
        return [...state, action.id]
    }else{
        return [
            ...state.slice(0,state.indexOf(action.id)),
            ...state.slice(state.indexOf(action.id)+1),
        ]
    }
}

export const useClothesLayer = () => {

    const sizeType = useRef(null)
    const [loading, setLoading] = useState(false)
    const [clothes, setClothes] = useState([])
    const [loadedClothes, setLoadedClothes] = useState([])
    const [selectedSizes, dispatchSelectedSizes] = useReducer(filterDispatcher, [])
    const [selectedColors, dispatchSelectedColors] = useReducer(filterDispatcher, [])

    const loadClothesByLayer = useCallback(async layer=>{
        setLoading(true)
        const response = await axios.get(`/rest/clothes.photos.list`, {
            params: {
                filter: {layer}
            },
            paramsSerializer: params => {
                return qs.stringify(params)
            }
        })
        let existingIds = []
        const clothes = response.data.result
            .map(v=>v.clothes[0])
            .filter(v=>!existingIds.includes(v.ID)&&existingIds.push(v.ID)&&v)
        setLoadedClothes(clothes)
        setLoading(false)
    },[])

    useEffect(()=>{
        setLoading(true)
        if(!selectedColors.length && !selectedSizes.length)
            setClothes(loadedClothes)
        else {
            const filteredClothes = loadedClothes.filter(clothes=>{
                if(selectedColors.length && selectedSizes.length)
                    return clothes.size.some(size => selectedSizes.includes(size.ID))*
                        clothes.color.some(color => selectedColors.includes(color.ID))
                if(selectedColors.length)
                    return clothes.color.some(color => selectedColors.includes(color.ID))
                if(selectedSizes.length)
                    return clothes.size.some(size => selectedSizes.includes(size.ID))

                return false
            })
            setClothes(filteredClothes)
        }
        sizeType.current = loadedClothes.length&&loadedClothes[0].size[0].type
        setLoading(false)
    }, [loadedClothes, selectedSizes, selectedColors])

    const sizeFilter = (id, state) => dispatchSelectedSizes({id,state})
    const colorFilter = (id, state) => dispatchSelectedColors({id, state})
    return {clothes, loadClothesByLayer, sizeFilter, selectedSizes, sizeType: sizeType.current, colorFilter, selectedColors, loading}
}