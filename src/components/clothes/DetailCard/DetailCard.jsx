import {DetailCardWrapper} from "./DetailCardWrapper";
import {addToCart, hideModal} from "../../../store/actions";
import {useDispatch, useSelector} from "react-redux";
import React, {useRef} from "react";
import {Button, Col, Row} from "react-materialize";
import {usePhotos} from "./usePhotos";
import {ModalDialog} from "../../ui/ModalDialog/ModalDialog";
import {useChangeClothes} from "../Catalog/Card/useChangeClothes";
import closeIcon from "../../../assets/img/closeIcon.svg";

export const DetailCard = props => {

    const selectedSize = useRef(null)
    const collections = useSelector(state=>state.collections)

    const availableSizes = props.size;
    const allSizes = collections.sizes[availableSizes[0].type]

    const dispatch = useDispatch()
    const modalState = useSelector(state=>state.modals[props.id]||false)

    const {tryChangeClothes, onApproveChanging} = useChangeClothes()
    const {photo, nextPhoto, prevPhoto} = usePhotos(props.photo)
    return <DetailCardWrapper
        actions={[]}
        id={props.id}
        open={modalState}
        options={{
            onCloseEnd: ()=>{
                dispatch(hideModal(props.id))
            }
        }}
        innerheight={window.innerHeight}
        photo={photo}>
        <ModalDialog
            id={`clothesChangeApprove_${props.ID}`}
            header="Продолжить?"
            onApprove={onApproveChanging}
        >
            У этой одежды отсутствуют фотографии на модели.
            При продолжении будет сгенерирован случайный лук с выбранной одеждой.
        </ModalDialog>
        <div className="contentWrapper">
            <div className="closeIcon modal-close">
                <img src={closeIcon} alt="close" />
            </div>
            <div className="gallery">
                <div className="photo" />
                <div className="controls">
                    <Button
                        className="white black-text waves-effect"
                        onClick={()=>prevPhoto()}>
                        Предыдущее фото
                    </Button>
                    <Button
                        className="white black-text waves-effect"
                        onClick={()=>nextPhoto()}>
                        Следующее фото
                    </Button>
                </div>
            </div>
            <div className="actions">
                <div className="cartActions">
                    <span className="productType">{props.category[0].name_single||props.category[0].name}</span>
                    <span className="productName">{props.name}</span>
                    <span className="productComponents">{props.clothes_composition.map(v=>v.name).join(', ')}</span>
                    <Row
                        className="productSizes"
                        onChange={e=>selectedSize.current = e.target.value}>
                        <span>Размеры в наличии</span>
                        {allSizes.map(allValue=>
                            <Col key={allValue.ID}>
                                <input
                                    disabled={!availableSizes.some(availableValue=>allValue.ID===availableValue.ID)}
                                    type="radio"
                                    value={allValue.ID}
                                    id={`cart_${props.ID}_${allValue.ID}`}
                                    name={`cart_${props.ID}`} />
                                <label htmlFor={`cart_${props.ID}_${allValue.ID}`}>
                                    <div>{allValue.size}</div>
                                </label>
                            </Col>
                        )}
                    </Row>
                    <span className="productPrice">
                        {Number(props.price).toLocaleString()}
                    </span>
                    <Button
                        className="waves-effect black white-text"
                        onClick={()=>dispatch(addToCart(props, allSizes.find(v=>v.ID===selectedSize.current)))}>
                        Добавить в корзину
                    </Button>
                </div>
                <div className="viewerActions">
                    <span>
                        {props.available_sizes.length?'Собрать образ с этой вещью':'Данную вещь нельзя примерить'}
                    </span>
                    <Row
                        className="viewerSizes"
                        onChange={e=>selectedSize.current = e.target.value}>
                        <span>Размеры на модели</span>
                        {props.available_sizes.map(size=>
                            <Col key={size.ID}>
                                <input
                                    disabled={true}
                                    type="radio"
                                    value={size.ID}
                                    id={`tryOn_${props.ID}_${size.ID}`}
                                    name={`tryOn_${props.ID}`} />
                                <label htmlFor={`tryOn_${props.ID}_${size.ID}`}>
                                    <div>{size.size}</div>
                                </label>
                            </Col>
                        )}
                    </Row>
                    <Button
                        className="waves-effect white black-text"
                        onClick={()=>tryChangeClothes(props.ID)}
                    >
                        Перейти к образу
                    </Button>
                </div>
            </div>
        </div>
    </DetailCardWrapper>
}