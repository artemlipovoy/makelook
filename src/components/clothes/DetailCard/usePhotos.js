import {useState} from "react";

export const usePhotos = photos => {
    const [curPhoto, setCurPhoto] = useState(typeof photos == "object"?0:null)
    let photo
    let nextPhoto = () => {}
    let prevPhoto = () => {}
    if(typeof photos == "object") {
        nextPhoto = () => (curPhoto !== null && curPhoto === photos.length - 1) ? setCurPhoto(0) : setCurPhoto(curPhoto + 1)
        prevPhoto = () => (curPhoto !== null && curPhoto === 0) ? setCurPhoto(photos.length - 1) : setCurPhoto(curPhoto - 1)
        photo = photos[curPhoto]
    }else{
        photo = photos
    }
    return {photo, nextPhoto, prevPhoto}
}