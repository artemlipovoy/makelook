import styled from "styled-components";
import {Modal} from "react-materialize";

export const DetailCardWrapper = styled(Modal)`
border: 1px solid black;
box-shadow: none;
border-radius: 0;
width: 752px;
height: 432px;
overflow: hidden;
max-height: unset;
background-color: #FFFFFF;

+ .modal-overlay {
    background: #ffffff;
    opacity: 0.64;
}
.modal-content {
    padding: 0;
    height: inherit;
}
.closeBtn i {
    font-size: 1.7rem;
    line-height: 0.5rem;
}
.closeBtn:active {
    background: none;
}
h4 {
    display: none;
}

.modal-footer {
    display: none;
}

.contentWrapper {
    height: 100%;
    margin: 0;
    display: flex;
    align-items: stretch;
    
    .closeIcon{
        display: block;
        cursor: pointer;
        position: relative;
        img{
            position: absolute;
            right: -752px;
        }
    }
    
    .gallery{
        width: 400px;
        border-right: 1px solid black;
        .photo {
            width: 100%;
            height: 400px;
            background-image: url("${props => props.photo}");
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
        .controls{
            display: flex;
            align-items: stretch;
            height: 32px;
            button{
                border-radius: 0;
                height: 32px;
                line-height: 32px;
                padding: 0;
                margin: 0;
                box-shadow: none;
                text-transform: none;
                width: 50%;
                border-top: 1px solid black;
                font-size: 12px; 
                font-weight: 300;
            }
            button:nth-child(1){
                border-right: 1px solid black;
            }
        }
    }
    .actions{
        width: 352px;
        .cartActions{
            height: 284px;   
            border-bottom: 1px solid black;
            padding: 16px;
            
            .productType{
                display: block;
                font-size: 12px;
                text-transform: uppercase;
                margin-bottom: 7px;
            }
            .productName{
                display: block;
                font-size: 20px;
                font-weight: 700;
                margin-bottom: 11px;
            }
            .productComponents{
                display: block;
                font-size: 14px;
            }
            .productSizes {
                margin: 16px 0;
                span {
                    display: block; 
                    text-transform: uppercase;
                    font-size: 10px;
                    color: #000000;
                    opacity: 0.64;
                }
            }
            .productSizes > .col {
                font-style: normal;
                font-size: 14px !important;
                font-weight: 300;
                margin: 8px 12px 8px 0;
                padding: 0;
            }
            .productSizes label > div {
                display: flex;
                align-items: center;
                justify-content: center;
                width: 30px;
                height: 30px;
                border: 1px solid #c7c7c7;
                color: black;
                font-size: 12px;
            }
            .productSizes input:checked + label > div {
                border: 1px solid black;
            }
            .productSizes input:disabled + label > div {
                background-image: linear-gradient(
                    to top right,
                    transparent 48%,
                    #c7c7c7,
                    transparent 52%
                );
                background-repeat: no-repeat;
                border: 1px solid #c7c7c7 !important;
                color: #c7c7c7;
            }
            .productPrice{
                font-weight: 300;
                font-size: 18px;
                text-transform: uppercase;
            }
            .productPrice::after {
                content: " ₽";
            }
            button{
                margin-top: 12px;
                display: block;
                text-transform: none!important;
                border-radius: 0;
                font-style: normal;
                font-weight: 300;
                box-shadow: none;
                font-size: 12px;
                padding: 8px 12px;
                line-height: unset;
                border: 1px solid black;
            
                :hover{
                    background-color: #FFFFFF!important;
                    color: #000000!important;
                }
            }
        }
        .viewerActions{
            height: 148px;
            padding: 16px;
            
            span{
                display: block;
                font-size: 10px;
                text-transform: uppercase;
                font-weight: 700;
            }
            
            .viewerSizes {
                margin: 4px 0;
                span {
                    display: block; 
                    text-transform: uppercase;
                    font-size: 10px;
                    color: #000000;
                    opacity: 0.64;
                    font-weight: 300;
                }
            }
            .viewerSizes > .col {
                font-style: normal;
                font-size: 14px !important;
                font-weight: 300;
                margin: 8px 12px 8px 0;
                padding: 0;
            }
            .viewerSizes label > div {
                display: flex;
                align-items: center;
                justify-content: center;
                width: 30px;
                height: 30px;
                border: 1px solid #c7c7c7;
                color: black;
                font-size: 12px;
            }
            .viewerSizes input:checked + label > div {
                border: 1px solid black;
            }
            button{
                display: block;
                text-transform: none!important;
                border-radius: 0;
                font-style: normal;
                font-weight: 300;
                box-shadow: none;
                font-size: 12px;
                padding: 8px 12px;
                line-height: unset;
                border: 1px solid black;
            
                :hover{
                    background-color: #000000!important;
                    color: #FFFFFF!important;
                }
            }
        }
    }
}

@media screen and (max-width: 751px) {
    top: 0!important;
    width: 100vw;
    height: ${props=>props.innerheight}px;
    overflow: auto;
    
    .contentWrapper{
        flex-direction: column;
        align-items: stretch;
        .closeIcon{
            img{
                right: 0;
            }
        }
        .gallery{
            width: 100%;
            border-right: none;
            .photo{
                height: 100vw;
            }
            .controls{
                button{border-bottom: 1px solid black;}
            }
        }
         .actions{
             width: unset!important;
         }
    }
}

`