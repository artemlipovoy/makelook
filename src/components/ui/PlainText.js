import styled from "styled-components";

export const PlainText = styled.span`
  font-size: 12px;
  ${props=>props.underline&&'text-decoration: underline;'}
  ${props=>props.pointer&&'cursor: pointer;'}
`
