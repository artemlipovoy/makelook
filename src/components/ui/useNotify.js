export const useNotify = () => {
    const DANGER_NOTIFY = {
        icon: 'error_outline',
        color: '#FF8484'
    }
    const SUCCESS_NOTIFY = {
        icon: 'check_circle',
        color: '#91FF84'
    }

    const INFO_NOTIFY = {
        icon: 'info',
        color: '#ffef84'
    }

    const notify = (msg, type) => {
        window.M.toast({html: `
        <span style="color: ${type.color}; margin-right: 1rem;" class="left material-icons">
            ${type.icon}
        </span>${msg}`})
    }

    return {
        notify,
        DANGER_NOTIFY,
        SUCCESS_NOTIFY,
        INFO_NOTIFY
    }
}