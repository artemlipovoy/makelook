import styled from "styled-components";
import {Container as SContainer} from "react-materialize";

export const Container = styled(SContainer)`
  @media screen and (max-width: 576px) {
      width: 100%;
      
  }
`