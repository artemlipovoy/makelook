import styled from 'styled-components'
import {ProgressBar as MPB} from "react-materialize";
import React from "react";

const Bar = styled(MPB)`
  margin: 0;
  background-color: #bbbbbb !important;
  .indeterminate{
    background-color: #000000 !important;
  }
`

const Wrapper = styled.div`
  position: relative;
  .progress{
    position: absolute;
  }
`

export const ProgressBar = () => <Wrapper><Bar /></Wrapper>