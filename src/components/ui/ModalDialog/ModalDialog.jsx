import React from 'react'
import {useDispatch, useSelector} from "react-redux";
import {Button} from "react-materialize";
import {hideModal} from "../../../store/actions";
import {ModalDialogWrapper} from "./ModalDialogWrapper";

export const ModalDialog = props => {

    const dispatch = useDispatch()
    const modalState = useSelector(state=>state.modals[props.id]||false)

    return <ModalDialogWrapper
        actions={[
            <Button
                flat
                modal="close"
                className="waves-effect black-text deny">
                Отмена
            </Button>,

            <Button
                onClick={()=>props.onApprove&&props.onApprove()}
                modal="close"
                className="waves-effect waves-light black white-text accept">
                Продолжить
            </Button>
        ]}
        header={props.header}
        id={props.id}
        open={modalState}
        options={{
            onCloseEnd: ()=>dispatch(hideModal(props.id))
        }}
    >
        <p>
            {props.children}
        </p>
    </ModalDialogWrapper>
}