import styled from "styled-components";
import {Modal} from "react-materialize";

export const ModalDialogWrapper = styled(Modal)`
  border: 1px solid black;
  box-shadow: none;

  + .modal-overlay{
     background: #FFFFFF;
  }

  button {
    text-transform: none!important;
    border-radius: 0;
    font-style: normal;
    font-weight: 300;
  }
`