import styled from "styled-components"
import {Preloader} from "react-materialize";

export const Loader = styled(Preloader)`
  width: 50px;
  height: 50px;
  
  ${props=> props.fixVertical ? `
  margin: 0 calc(50% - 25px);
  ` : `
  margin: calc(50% - 25px);`}
  
  
  .spinner-blue, .spinner-blue-only{
    border-color: #000000;
  }
`