import {useCallback, useState} from "react";
import jsonToFormData from "json-form-data"
import axios from 'axios'

export const useCart = () => {
    const [isShow, setIsShow] = useState(false)
    const [loading, setLoading] = useState(false)
    const [orderDetails, setOrderDetails] = useState({
        completed: false,
        contactId: null,
        dealId: null
    })
    const toggleCredential = () => setIsShow(!isShow)

    const sendOrder = useCallback(async (credential, order, totalSum)=>{
        setLoading(true)
        const response = await axios.post(`/rest/bitrix24.createOrder`, jsonToFormData({
            products: order,
            contact: credential,
            opportunity: totalSum
        }))
        const orderResult = response.data.result
        setOrderDetails({
            completed: true,
            ...orderResult
        })
        setLoading(false)
        setTimeout(()=>window.location.reload(), 1500)
    },[])

    return {isShow, toggleCredential, sendOrder, loading, orderDetails}
}