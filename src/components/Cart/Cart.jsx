import {CartWrapper} from "./CartWrapper";
import {hideModal} from "../../store/actions";
import {useDispatch, useSelector} from "react-redux";
import React, {useRef} from "react";
import {Button, Col, Icon, TextInput} from "react-materialize";
import InputMask from 'react-input-mask';
import {CartElement} from "./CartElement";
import {PlainText} from "../ui/PlainText";
import {CredentialForm} from "./CredentialForm";
import {useCart} from "./useCart";
import {Loader} from "../ui/Loader";
import closeIcon from "../../assets/img/closeIcon.svg";

export const Cart = () => {
    const dispatch = useDispatch()
    const credentialForm = useRef({
        name: '',
        phone: '',
        addr: ''
    })

    const modalState = useSelector(state=>state.modals.cart||false)
    const cartItems = useSelector(state=>Object.values(state.cart.items)||[])
    const totalSum = useSelector(state=>state.cart.totalSum||0)

    const {isShow, toggleCredential, sendOrder, loading, orderDetails} = useCart()

    return <CartWrapper
        actions={[]}
        id="cart"
        open={modalState}
        viewheight={window.innerHeight}
        options={{
            onCloseEnd: ()=>{
                dispatch(hideModal("cart"))
            }
        }}>
        {loading?
            <Loader />:
            <>
                {orderDetails.completed?<>
                        <h3><Icon large>check</Icon></h3>
                        <h1>
                            Номер вашего заказа <b>{`${orderDetails.contactId}${orderDetails.dealId}`}</b>
                        </h1>
                        <h2>
                            Скоро наш оператор свяжется с вами для подтверждения
                        </h2>
                    </>:
                    <>
                        <div className="closeIcon modal-close">
                            <img src={closeIcon} alt="close" />
                        </div>
                        <span className="cartTitle">Корзина</span>
                        {cartItems.map(item=><CartElement key={item.product.ID} {...item} />)}
                        <div className="infoMessage">
                            <h6>
                                Доставка
                            </h6>
                            В течении 4-ёх часов при заказе  10 до 18, или на следующий день при заказе после 18.
                            <span className="right">500 ₽</span>
                        </div>

                        <div className="right-align">
                            <h5>
                                <PlainText className="totalSum">Все вместе получается {(totalSum+500).toLocaleString()} ₽</PlainText>
                            </h5>
                            <div>
                                <Button
                                    onClick={toggleCredential}
                                    className="waves-effect waves-light black white-text cartCheckOut">
                                    {isShow?'Назад к корзине':'Перейти к оформлению'}
                                </Button>
                            </div>
                        </div>

                        <CredentialForm show={isShow?1:0}>
                            <span>Данные для заказа</span>
                            <Col s={12} m={8}>
                                <form onSubmit={e=>{
                                    e.preventDefault()
                                    sendOrder(credentialForm.current, cartItems, totalSum+500)
                                }}>
                                    <TextInput
                                        s={12}
                                        m={6}
                                        onChange={e=>credentialForm.current={...credentialForm.current, [e.target.id]: e.target.value}}
                                        required={true}
                                        id="name"
                                        label="Имя"
                                        placeholder="Иван Иванов" />
                                    <InputMask
                                        onChange={e=>credentialForm.current={...credentialForm.current, [e.target.id]: e.target.value}}
                                        mask="+7 (999) 999-99-99">
                                        {()=><TextInput
                                            s={12}
                                            m={6}
                                            required={true}
                                            id="phone"
                                            label="Телефон"
                                            placeholder="+7 (999) 999-99-99" />}
                                    </InputMask>
                                    <TextInput
                                        s={12}
                                        onChange={e=>credentialForm.current={...credentialForm.current, [e.target.id]: e.target.value}}
                                        required={true}
                                        id="addr"
                                        label="Адрес"
                                        placeholder="г. Кудрово, ул. Шишкова, д. 32, кв. 50" />
                                    <Button
                                        className="waves-effect black-text white cartCheckOut">
                                        Оформить
                                    </Button>
                                </form>
                            </Col>
                        </CredentialForm>
                    </>
                }
            </>
        }
    </CartWrapper>
}