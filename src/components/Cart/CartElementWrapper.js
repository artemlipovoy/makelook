import styled from "styled-components";
import {Row} from "react-materialize";

export const CartElementWrapper = styled(Row)`
  padding: 0;
  margin: 15px 0;
  height: 120px;
  border: 1px solid black;
  display: flex;
  align-items: stretch;
  background: #FFFFFF;
  .cartProductCover{
    background-image: url("${props=>props.cover}");
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    width: 120px;
  }
  
  .cartProductDescription{
    padding: 10px;
    flex-grow: 4;
    display: flex;
    flex-direction: column;
    justify-content: center;
    
    h6{  
        font-style: normal;
        font-weight: 300;
        font-size: 11px!important;
        text-transform: uppercase;
        
        margin: 5px 0;
        padding: 0;
    }
    
    .cartProductName{
        margin: 0;
        padding: 0;
    }
    
    .cartProductSize{
        margin: 10px 0;
    
        display: flex;
        align-items: center;
        justify-content: center;
        width: 30px;
        height: 30px;
        border: 1px solid #c7c7c7;
        color: black;
        font-size: 12px;
    }
  }
  
  .cartProductQuantity{
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-end;
    flex-grow: 1;
    padding: 10px;
    
    h5{
        margin: 0;
        padding: 8px 0;
        font-style: normal;
        font-weight: 300;
        font-size: 1.2em;
        text-transform: uppercase;
    }
    h5::after{
        content: ' ₽';
    }
    
    .cartProductQuantitySelector > .col {
        font-style: normal;
        font-weight: 300;
        margin: 4px 0 4px 4px;
        padding: 0;
    }
      
    .cartProductQuantitySelector label > div {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 30px;
        height: 30px;
        border: 1px solid #c7c7c7;
        color: black;
        font-size: 12px;
    }
      
    .cartProductQuantitySelector input:checked + label > div{
        border: 1px solid black;
    }
  }
`