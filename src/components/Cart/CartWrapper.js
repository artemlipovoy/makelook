import styled from "styled-components";
import {Modal} from "react-materialize";


export const CartWrapper = styled(Modal)`
    border: 1px solid black;
    border-radius: 0;
    background-color: #FFFFFF;
    box-shadow: none;
    
    + .modal-overlay{
      background-color: #FFFFFF;
      opacity: 0.64;
    }
    
    .modal-content{
      h1{
        text-align: center;
        font-weight: 300;
        font-size: 20px;
        text-transform: uppercase;
      }
      h2{
        text-align: center;
        font-weight: 300;
        font-size: 12px;
        text-transform: uppercase;
      }
      h3{
        text-align: center;
        font-size: 20px;
      }
      
    
      h4{display: none;}
      
      padding: 16px;
      
      .cartTitle{
        display: block;       
         font-size: 20px;
         font-weight: bold;
      }
      
      .closeIcon{
        display: block;
        cursor: pointer;
        position: relative;
            img{
                position: absolute;
                right: 0;
            }
        }
      
      h6{  
             margin: 0 0 8px 0; 
             font-style: normal;
             font-weight: 300;
             font-size: 11px!important;
             text-transform: uppercase;
      }
      .infoMessage{
         border: 1px solid black;
         padding: 12px;
      }
      
    }
    .modal-footer{display: none;}
    .totalSum{
        font-size: 15px;
    }
    .cartCheckOut{
        text-transform: none!important;
        border-radius: 0;
        font-style: normal;
        font-weight: 300;
        border: 1px solid black;
    }
    
     button{
        text-transform: none!important;
        border-radius: 0;
        font-style: normal;
        font-weight: 300;
        box-shadow: none;
        font-size: 12px;
        padding: 8px 12px;
        line-height: unset;
        border: 1px solid black;
        :hover{
            background-color: #FFFFFF!important;
            color: #000000!important;
        }
    }
    
    @media screen and (max-width: 751px){
         width: 100vw;
         height: ${props=>props.viewheight}px;
         max-height: unset;
         top: 0!important;
        .modal-content{
             .infoMessage{
                height: 100px;
             }
        }
    }
`