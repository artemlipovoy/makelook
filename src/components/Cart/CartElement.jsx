import React from "react";
import {CartElementWrapper} from "./CartElementWrapper";
import {PlainText} from "../ui/PlainText";
import {Col} from "react-materialize";
import {changeQuantity} from "../../store/actions";
import {useDispatch} from "react-redux";

export const CartElement = props => {
    const availableQuantity = [0,1,2]
    const dispatch = useDispatch()

    return <CartElementWrapper cover={typeof props.product.photo=='object'?props.product.photo[0]:props.product.photo}>
        <div className="cartProductCover hide-on-small-only" />
        <div className="cartProductDescription">
            <h6>{props.product.category[0].name_single||props.product.category[0].name}</h6>
            <div className="cartProductName"><PlainText>{props.product.name}</PlainText></div>
            <div>
                <span className="cartProductSize">
                    {props.size.size}
                </span>
            </div>
        </div>
        <div className="cartProductQuantity right-align">
            <div><PlainText>Количество вещей</PlainText></div>
            <div className="cartProductQuantitySelector">
                {availableQuantity.map(q=><Col key={`q_${q}`}>
                    <input
                        type="radio"
                        onChange={e=>dispatch(changeQuantity(props.product.ID, e.target.value))}
                        defaultChecked={props.quantity===q}
                        value={q}
                        id={`product_${props.product.ID}_q_${q}`}
                        name={`product_${props.product.ID}_q`}
                    />
                    <label htmlFor={`product_${props.product.ID}_q_${q}`}>
                        <div>{q}</div>
                    </label>
                </Col>)}
            </div>
            <h5>{(props.product.price*props.quantity).toLocaleString()}</h5>
        </div>
    </CartElementWrapper>
}