import styled from "styled-components";
import {Row} from "react-materialize";

export const CredentialForm = styled(Row)`
  padding: 0;
  margin: 16px 0 0;
  height: 0;
  width: 100%;
  overflow: hidden;
  transition: height 0.5s linear;
  
  span{
      padding-top: 12px;
      border-top: 1px solid black;
      display: block;
      font-size: 15px;
      font-weight: 700;
      margin-bottom: 12px;
  }
  
  > .col{
    padding: 0;
  }
  
  ${state => state.show?`
    height: 260px;
  `:undefined}
  
  input{
    margin-left: -6px!important;
    margin-bottom: 0;
    padding:0 6px!important;
  }
  
  .input-field{
    padding: 0 15px 0 6px;
  }
  
  input {border: 1px solid #9e9e9e!important;}
  input + label {color: #9e9e9e!important;}
    
  input:focus + label {color: #000000!important;}
  input:focus {
    border: 1px solid #000000!important;
    box-shadow: 0 1px 0 0 #000000!important;
  }

  .input-field>label:not(.label-icon).active{
    transform: translateY(-20px) scale(0.8);
  }  
  
  button:hover{
      background-color: #000000!important;
      color: #FFFFFF!important;
  }
  
  @media screen and (max-width: 600px){
         ${state => state.show?`
          height: 330px;
        `:undefined}      
  }
`
