import React from 'react';
import ReactDOM from 'react-dom';
import 'materialize-css';
import './index.css';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import App from './App';
import {appReducer} from "./store/reducers/app.reducer";
import {clothesReducer} from "./store/reducers/clothes.reducer";
import {collectionsReducer} from "./store/reducers/collections.reducer";
import {modalReducer} from "./store/reducers/modal.reducer";
import {cartReducer} from "./store/reducers/cart.reducer";

const store = createStore(combineReducers({
        app: appReducer,
        clothes: clothesReducer,
        collections: collectionsReducer,
        modals: modalReducer,
        cart: cartReducer
    }),
    compose(
        applyMiddleware(
            thunk
        )
    ))

ReactDOM.render(
    <Provider
        store={store}
    >
        <App />
    </Provider>,
    document.getElementById('root')
);
